create database abarrotespueblito;
use abarrotespueblito;

#region Tablas
create table proveedor(idproveedor int primary key AUTO_INCREMENT, nombrep varchar(50), direccion varchar(100), telefono varchar(20));
create table categoria(idcategoria int primary key AUTO_INCREMENT, nombrec varchar(20));
create table producto(idproducto int primary key AUTO_INCREMENT, nombrepd varchar(50), precio double, fkidcategoria int, foreign key(fkidcategoria)
references categoria(idcategoria));
create table tienda(idtienda int primary key AUTO_INCREMENT, cantidad int, fecha varchar(20), fkidproveedor int, fkidproducto int, foreign key(fkidproveedor)
references proveedor(idproveedor), foreign key(fkidproducto) references producto(idproducto));
#end region
alter table tienda modify fecha varchar(20);

#region Vista
create view v_surtido as select nombrep as Proveedor, nombrec as Categoria, nombrepd as Producto, precio as Precio, cantidad, fecha from proveedor, producto, categoria, tienda
where producto.fkidcategoria = categoria.idcategoria and tienda.fkidproveedor=proveedor.idproveedor and tienda.fkidproducto=producto.idproducto;

select * from v_surtido;

select * from categoria;
#end region

#region Datos ingresas en tablas
insert into proveedor values(null,'Coca-Cola', 'Boulevard Feliz Ramirez #334','474-545-5848');
insert into categoria values(null, 'Refresco');
insert into categoria values(null, 'Pan Dulce');
insert into producto values(null, 'Fanta', 12.00,1);
insert into tienda values(null, 15, "12-02-2020",1,1);

#end region

#region Procedure
drop procedure if exists inserttienda;
 create procedure inserttienda(in _cantidad int, in _fecha varchar(20), in _fkidproveedor int, in _fkidproducto int)
 begin
 declare x int;
 if  _cantidad >=1 then
 insert into tienda values(null, _cantidad, _fecha, _fkidproveedor, _fkidproducto);
 else
 select "Cantidad necesaria";
 end if;
 end;
 
 
 call inserttienda(20,'15-02-2020',1,1);
 select * from tienda;
 #end region
 
 #region consulta en orden
 select nombrepd, precio, nombrec from producto, categoria where fkidcategoria = idcategoria order by nombrec;
 #end region